$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover({
		trigger: 'hover'
	});
	$('.carousel').carousel({
		interval: 3000
	});
	$("#modal-contacto").on('show.bs.modal', function(e){
		console.log('El modal se esta mostrando');
		$("#btn-contacto").removeClass("btn-success");
		$("#btn-contacto").addClass("btn-primary");
		$("#btn-contacto").prop('disabled',true);
	});
	$("#modal-contacto").on('shown.bs.modal', function(e){
		console.log('El modal se mostro');
	});
	$("#modal-contacto").on('hide.bs.modal', function(e){
		console.log('El modal se esta ocultando');
		$("#btn-contacto").removeClass("btn-primary");
		$("#btn-contacto").addClass("btn-success");
		$("#btn-contacto").prop('disabled',false);
	});
	$("#modal-contacto").on('hidden.bs.modal', function(e){
		console.log('El modal se oculto');
	});
});